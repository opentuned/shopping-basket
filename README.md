# shopping-basket

Technical exercise for an interview calculating price of fruit with certain offers on applied various quantities.


## Spec

Using Java, write a simple program that calculates the price of a basket of shopping.
The solution should be accomplished in roughly two hours but please don’t rush the exercise,
take the time you need.

Items are presented one at a time, in a list, identified by name - for example "Apple" or
"Banana".

Multiple items are present multiple times in the list, so for example ["Apple", "Apple", "Banana"]
is a basket with two apples and one banana.

Items are priced as follows:

- Apples are 35p each
- Bananas are 20p each
- Melons are 50p each, but are available as - buy one get one free?
- Limes are 15p each, but are available in a - three for the price of two? offer

Given a list of shopping, calculate the total cost of those items.
Ask the candidates to upload the solution to BitBucket (they create their own repository) and
share the link with us to download it.

## Solution

- Written against JDK 11, should work with 9+ due to use of `List.of`
- Only written against tests, ie no main method to run.
- Uses MVN as a build tool, use the following to run tests `mvn test`
- With more time further changes would included:
    
    - Consider money type more, prices are all in pence...
    - More consideration around exception handling, and logging 
    - A pattern for encapsulating offer logic for reuse outside of class
    - Consideration for concurrency