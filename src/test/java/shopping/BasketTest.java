package shopping;

import org.junit.Test;

import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static shopping.Basket.*;

/* Unit testing strategy

Z.ero
O.ne
M.any
B.oundaries
I.nterfaces
E.xcpetions
S.imple

 */

public class BasketTest {

    @Test
    public void ZeroTestCase() {
        assertThat(calculateCost(emptyList()), equalTo(0));
    }

    @Test
    public void OneApple() {
        assertThat(calculateCost(List.of("Apple")), equalTo(35));
    }

    @Test
    public void OneBanana() {
        assertThat(calculateCost(List.of("Banana")), equalTo(20));
    }

    @Test
    public void OneMelon() {
        assertThat(calculateCost(List.of("Melon")), equalTo(50));
    }

    @Test
    public void OneLime() {
        assertThat(calculateCost(List.of("Lime")), equalTo(15));
    }

    @Test
    public void OneOfEverythingAppleBananaMelonAndLime() {
        assertThat(calculateCost(List.of("Apple","Banana", "Melon", "Lime")), equalTo(120));
    }

    @Test
    public void TwoMelonsForThePriceOfOneGetsOneFree() {
        assertThat(calculateCost(List.of("Melon", "Melon")), equalTo(50));
    }

    @Test
    public void ThreeMelonsOnlyGetsOneFree() {
        assertThat(calculateCost(List.of("Melon", "Melon", "Melon")), equalTo(100));
    }

    @Test
    public void FourMelonsOnlyGetsTwoFree() {
        assertThat(calculateCost(List.of("Melon", "Melon", "Melon", "Melon")), equalTo(100));
    }

    @Test
    public void TwoLimesNoDiscount() {
        assertThat(calculateCost(List.of("Lime", "Lime")), equalTo(30));
    }

    @Test
    public void ThreeLimesGetsDiscount() {
        assertThat(calculateCost(List.of("Lime", "Lime", "Lime")), equalTo(30));
    }

    @Test
    public void FourLimesGetsDiscountForThree() {
        assertThat(calculateCost(List.of("Lime", "Lime", "Lime", "Lime")), equalTo(45));
    }

    @Test
    public void FiveLimesGetsDiscountForThree() {
        assertThat(calculateCost(List.of("Lime", "Lime", "Lime", "Lime", "Lime")), equalTo(60));
    }

    @Test
    public void SixLimesGetsDiscountForThreeAppliedTwice() {
        assertThat(calculateCost(List.of("Lime", "Lime", "Lime", "Lime", "Lime")), equalTo(60));
    }

    @Test
    public void VariousItemsGetsMelonAndLimeDeals() {
        assertThat(calculateCost(List.of("Apple","Banana", "Melon", "Melon", "Melon", "Melon", "Lime", "Lime", "Lime", "Lime", "Lime")), equalTo(215));
    }

    @Test
    public void BasicProvidedTestCase() {
        assertThat(calculateCost(List.of("Apple", "Apple", "Banana")), equalTo(90));
    }

    @Test
    public void Boundaries_ItemsAreCaseInsensitive() {
        assertThat(calculateCost(List.of("baNaNa", "ApPlE", "LIMe", "LImE", "MeLOn", "MeLoN")), equalTo(135));
    }

    @Test
    public void Boundaries_ItemsContainSpellingMistakes() {
        assertThat(calculateCost(List.of("baNaNas", "ApPlEZ", "LIMee", "LImEs", "Me1On", "MeL0N")), equalTo(0));
    }

    @Test
    public void Boundaries_MalformedInput_UnknownItem() {
        assertThat(calculateCost(List.of("BunchOfGrapes")), equalTo(0));
    }

    @Test
    public void Boundaries_MalformedInput_UnknownItemAndKnownItemsDoesntThrowError() {
        assertThat(calculateCost(List.of("BunchOfGrapes", "Apple")), equalTo(35));
    }

    @Test
    public void Exceptions_HighLoadList() {
        int noOfItems = 1000000;
        long start = System.nanoTime();
        System.out.println("Start: " + start);
        List<String> items = java.util.stream.IntStream.range(0, noOfItems).mapToObj(n -> "Apple").collect(toList());
        assertThat(calculateCost(items), equalTo(noOfItems * APPLE_PRICE));
        long stop = System.nanoTime();
        System.out.println("Stop: " + stop);
        System.out.println("Ex time: " + (stop - start));
    }

}
