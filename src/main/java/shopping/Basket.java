package shopping;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Basket {

    public static int APPLE_PRICE = 35;
    public static int BANANA_PRICE = 20;
    public static int MELON_PRICE = 50;
    public static int LIME_PRICE = 15;

    public static int calculateCost(List<String> items) {

        int totalCost = 0;
        Map<Fruit, Integer> itemMap = countItems(items);

        for(Fruit key: itemMap.keySet()) {
            switch(key) {
                case APPLE:
                    totalCost += itemMap.get(key) * APPLE_PRICE;
                    break;
                case BANANA:
                    totalCost += itemMap.get(key) * BANANA_PRICE;
                    break;
                case MELON:
                    totalCost += offerTwoForOne(itemMap.get(key), MELON_PRICE);
                    break;
                case LIME:
                    totalCost += offerThreeForTwo(itemMap.get(key), LIME_PRICE);
                    break;
                default:
                    break;
            }
        }
        return totalCost;
    }

    private static Map<Fruit, Integer> countItems(List<String> items) {

        Map<Fruit, Integer> itemMap = new HashMap<>();

        for(String item : items) {
            try{
                Fruit castItem = Fruit.valueOf(item.toUpperCase());
                switch(castItem) {
                    case APPLE:
                        itemMap.merge(Fruit.APPLE, 1, Integer::sum);
                        break;
                    case BANANA:
                        itemMap.merge(Fruit.BANANA, 1, Integer::sum);
                        break;
                    case MELON:
                        itemMap.merge(Fruit.MELON, 1, Integer::sum);
                        break;
                    case LIME:
                        itemMap.merge(Fruit.LIME, 1, Integer::sum);
                        break;
                    default:
                        break;
                }
            } catch (IllegalArgumentException e) {
                // Log at appropriate level
                System.out.println("Unknown item " + e.getMessage() + ", will not be processed or alter total price");
            }
        }
        return itemMap;
    }

    private enum Fruit {
        APPLE,
        BANANA,
        MELON,
        LIME
    }

    private static int offerTwoForOne(int items, int itemRegularPrice) {
        int cost = 0;
        if(items % 2 == 0) {
            cost += (items / 2 ) * itemRegularPrice;
        } else {
            cost += (((items  -1 ) / 2 ) * itemRegularPrice ) + itemRegularPrice;
        }
        return cost;
    }

    private static int offerThreeForTwo(int items, int itemRegularPrice) {
        int cost = 0;
        if(items >= 3) {
            int countsOfThree = items / 3;
            int countNotEligible = items % 3;
            cost += ((countsOfThree * 2) * itemRegularPrice) + countNotEligible * itemRegularPrice;
        }
        else {
            cost += items * itemRegularPrice;
        }
        return cost;
    }

}
